<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
<!-- <!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body> -->
        <h2 style="margin-top:0px">Barang List</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('barang/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('barang/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('barang'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Merk</th>
		<th>Ukuran</th>
		<th>Jenis</th>
		<th>Warna</th>
		<!-- <th>Created At</th> -->
		<!-- <th>Updated At</th> -->
		<th>Action</th>
            </tr><?php
            foreach ($barang_data as $barang)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $barang->merk ?></td>
			<td><?php echo $barang->ukuran ?></td>
			<td><?php echo $barang->jenis ?></td>
			<td><?php echo $barang->warna ?></td>
			<!-- <td><?php echo $barang->created_at ?></td> -->
			<!-- <td><?php echo $barang->updated_at ?></td> -->
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('barang/read/'.$barang->id),'Read'); 
				echo ' | '; 
				echo anchor(site_url('barang/update/'.$barang->id),'Update'); 
				echo ' | '; 
				echo anchor(site_url('barang/delete/'.$barang->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
		<?php echo anchor(site_url('barang/excel'), 'Excel', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('barang/word'), 'Word', 'class="btn btn-primary"'); ?>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    <!-- </body>
</html> -->
          <?php $this->load->view('footer');?>
