<!-- partial:../../partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="nav-profile-image">
                  <img src="<?php echo base_url('assets');?>/images/faces/face1.jpg" alt="profile">
                  <span class="login-status online"></span>
                  <!--change to offline or busy as needed-->
                </div>
                <div class="nav-profile-text d-flex flex-column">
                  <span class="font-weight-bold mb-2"><?php echo $this->session->userdata('nama_lengkap');?></span>
                  <span class="text-secondary text-small"><?php if($this->session->userdata('akses') == 1){
                    echo "Administrator";
                  }elseif($this->session->userdata('akses') == 2){
                    echo "Operator";
                  }elseif($this->session->userdata('akses') == 3){
                    echo "Penjaga";
                  }elseif($this->session->userdata('akses') == 4){
                    echo "Kepala Gudang";
                  }?></span>
                </div>
                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
              </a>
            </li>
            <?php 
              if($this->session->userdata('akses') == 1){ ?>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url('Pengguna');?>">
                    <span class="menu-title">Pengguna</span>
                    <i class="mdi mdi-home menu-icon"></i>
                  </a>
                </li>
              <?php }elseif($this->session->userdata('akses') == 2){
            ?>
            
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('Barang');?>">
                <span class="menu-title">Barang</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
          <?php }elseif($this->session->userdata('akses') == 3){
            ?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('Daftarhadir');?>">
                <span class="menu-title">Daftar Hadir</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
          <?php }elseif($this->session->userdata('akses') == 4){?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('Report/barang');?>">
                <span class="menu-title">Barang</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('Report/Pengguna');?>">
                <span class="menu-title">Pengguna</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('Report/daftarhadir');?>">
                <span class="menu-title">Daftar Hadir</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
          <?php }elseif($this->session->userdata('akses') == 5){?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('Request');?>">
                <span class="menu-title">Form Request</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
          <?php } ?>
          </ul>
        </nav>