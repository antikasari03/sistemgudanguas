<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Pengguna List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Username</th>
		<th>Password</th>
		<th>Nama Lengkap</th>
		<th>Alamat</th>
		<th>Notelp</th>
		<th>Akses</th>
		<th>Created At</th>
		<th>Updated At</th>
		
            </tr><?php
            foreach ($pengguna_data as $pengguna)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $pengguna->username ?></td>
		      <td><?php echo $pengguna->password ?></td>
		      <td><?php echo $pengguna->nama_lengkap ?></td>
		      <td><?php echo $pengguna->alamat ?></td>
		      <td><?php echo $pengguna->notelp ?></td>
		      <td><?php if($pengguna->akses == 1){echo "Administrator";}elseif($pengguna->akses == 2){echo "Operator";}elseif($pengguna->akses == 3){echo "Penjaga";}elseif($pengguna->akses == 4){echo "Kepala Gudang";}elseif($pengguna->akses == 5){echo "Departemen Penjualan";} ?></td>
		      <td><?php echo $pengguna->created_at ?></td>
		      <td><?php echo $pengguna->updated_at ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>