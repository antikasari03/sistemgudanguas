<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <?php $this->load->view('sidebar');?>
        <div class="main-panel">
          <div class="content-wrapper">
<!-- <html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/> -->
        <!-- <style>
            body{
                padding: 15px;
            }
        </style> -->
    <!-- </head>
    <body> -->
        <h2 style="margin-top:0px">Pengguna <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Username <?php echo form_error('username') ?></label>
            <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
        </div>
        <?php if($button == "Create"){?>
	    <div class="form-group">
            <label for="varchar">Password <?php echo form_error('password') ?></label>
            <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
        </div>
        <?php }else{?>
            <input type="hidden" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
        <?php } ?>
	    <div class="form-group">
            <label for="varchar">Nama Lengkap <?php echo form_error('nama_lengkap') ?></label>
            <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap" value="<?php echo $nama_lengkap; ?>" />
        </div>
	    <div class="form-group">
            <label for="alamat">Alamat <?php echo form_error('alamat') ?></label>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Notelp <?php echo form_error('notelp') ?></label>
            <input type="text" class="form-control" name="notelp" id="notelp" placeholder="Notelp" value="<?php echo $notelp; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Akses <?php echo form_error('akses') ?></label>
            <select class="form-control" name="akses" id="akses" placeholder="Akses" >
                <?php if($button == "Create"){
                    echo "<option value='1'>Administrator</option><option value='2'>Operator</option><option value='3'>Penjaga Gudang</option><option value='4'>Kepala Gudang</option><option value='5'>Departemen Penjualan</option>";
                }else{
                    if($akses == 1){$dakses =  "Administrator";}
                    elseif($akses == 2){$dakses =  "Operator";}
                    elseif($akses == 3){$dakses =  "Penjaga";}
                    elseif($akses == 4){$dakses =  "Kepala Gudang";}
                    elseif($akses == 5){$dakses =  "Departemen Penjualan";}
                    echo "<option value=".$akses." selected>".$dakses."</option><option value='1'>Administrator</option>
                    <option value='2'>Operator</option>
                    <option value='3'>Penjaga Gudang</option>
                    <option value='4'>Kepala Gudang</option>
                    <option value='5'>Departemen Penjualan</option>";
                }?>
                
            </select>
            <!-- <input type="text" class="form-control" name="akses" id="akses" placeholder="Akses" value="<?php echo $akses; ?>" /> -->
        </div>
	    <!-- <div class="form-group">
            <label for="timestamp">Created At <?php echo form_error('created_at') ?></label>
            <input type="text" class="form-control" name="created_at" id="created_at" placeholder="Created At" value="<?php echo $created_at; ?>" />
        </div> -->
	    <!-- <div class="form-group">
            <label for="timestamp">Updated At <?php echo form_error('updated_at') ?></label>
            <input type="text" class="form-control" name="updated_at" id="updated_at" placeholder="Updated At" value="<?php echo $updated_at; ?>" />
        </div> -->
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('pengguna') ?>" class="btn btn-default">Cancel</a>
	</form>
    <!-- </body>
</html> -->
          <?php $this->load->view('footer');?>
