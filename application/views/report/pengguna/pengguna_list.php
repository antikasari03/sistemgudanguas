<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <?php $this->load->view('sidebar');?>
        <div class="main-panel">
          <div class="content-wrapper">
<!-- <!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body> -->
        <h2 style="margin-top:0px">Pengguna List</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('pengguna/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('pengguna/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('pengguna'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Username</th>
		<!-- <th>Password</th> -->
		<th>Nama Lengkap</th>
		<th>Alamat</th>
		<th>Notelp</th>
		<th>Akses</th>
		<!-- <th>Created At</th> -->
		<!-- <th>Updated At</th> -->
		<th>Action</th>
            </tr><?php
            foreach ($pengguna_data as $pengguna)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $pengguna->username ?></td>
			<!-- <td><?php echo $pengguna->password ?></td> -->
			<td><?php echo $pengguna->nama_lengkap ?></td>
			<td><?php echo $pengguna->alamat ?></td>
			<td><?php echo $pengguna->notelp ?></td>
			<td><?php if($pengguna->akses == 1){echo "Administrator";}elseif($pengguna->akses == 2){echo "Operator";}elseif($pengguna->akses == 3){echo "Penjaga";}elseif($pengguna->akses == 4){echo "Kepala Gudang";}elseif($pengguna->akses == 5){echo "Departemen Penjualan";} ?></td>
			<!-- <td><?php echo $pengguna->created_at ?></td> -->
			<!-- <td><?php echo $pengguna->updated_at ?></td> -->
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('pengguna/read/'.$pengguna->id),'Read'); 
				echo ' | '; 
				echo anchor(site_url('pengguna/update/'.$pengguna->id),'Update'); 
				echo ' | '; 
				echo anchor(site_url('pengguna/delete/'.$pengguna->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
		<?php echo anchor(site_url('pengguna/excel'), 'Excel', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('pengguna/word'), 'Word', 'class="btn btn-primary"'); ?>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    <!-- </body>
</html> -->
          <?php $this->load->view('footer');?>
