<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Daftarhadir List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Pengguna</th>
		<th>Status</th>
		<th>Created At</th>
		<th>Updated At</th>
		
            </tr><?php
            foreach ($daftarhadir_data as $daftarhadir)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $daftarhadir->id_pengguna ?></td>
		      <td><?php echo $daftarhadir->status ?></td>
		      <td><?php echo $daftarhadir->created_at ?></td>
		      <td><?php echo $daftarhadir->updated_at ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>